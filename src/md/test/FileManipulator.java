package md.test;

import java.io.*;

public class FileManipulator {
    private String fileName;
    private File file;
    private String fileMode;
    private FileWriter fw;
    private FileReader fr;
    private BufferedWriter bw;
    private BufferedReader br;

    FileManipulator(String fileName, String fileMode) throws IOException {
        this.fileName = fileName;
        this.fileMode = fileMode;
        file = new File(fileName);
        if (fileMode == "w") {
            this.fw = new FileWriter(this.file.getAbsoluteFile());
            this.bw = new BufferedWriter(this.fw);
        } else {
            this.fr = new FileReader(this.file.getAbsoluteFile());
            this.br = new BufferedReader(fr);
        }
    }

    public void write(String outText) throws IOException {
        bw.write(outText);
    }

    public String readline() throws IOException {
        return this.br.readLine();
    }

    public void close() throws IOException {
        if (fileMode == "w") {
            this.bw.close();
            this.fw.close();

        } else {
            this.br.close();
            this.fr.close();

        }
    }


}
